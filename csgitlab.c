#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main() {

  char gitName[30];
  char username[30];
  char email[100];
  char csgitlabLink[300];
  char command[150];
  char buf[BUFSIZ];
  int choice = 0;
  bool csgitlab = true;

  while (csgitlab) {

    strcpy(command, "clear");
    system(command);

/** Prompts user to configure git for CSGitlab */

    printf("\n");
    printf("CSGitlab:\n");
    printf("1: Add/change email\n");
    printf("2: Add/change username\n");
    printf("3: Add CSGitlab address\n");
    printf("4: Change CSGitlab address\n");
    printf("5: Push git repository to remote repository\n");
    printf("6: Pull remote repository to current repository\n");
    printf("7: Home\n");

    scanf("%d", &choice);

    system(command);

/** Prompts user to configure email */

    if (choice == 1) {

      printf("Please enter your email: ");
      scanf("%s", email);

/** bash command for setting email for git **/

      snprintf(command, sizeof(buf), "git config --global user.email \"%s\"", email);
      system(command);

/** bash command for saving user details for convenience in cache **/

      strcpy(command, "git config --global credential.helper cache");
      system(command);

    }

    if (choice == 2) {

/** Prompts user to enter their username **/

      printf("Please enter your username: ");
      scanf("%s", username);

/** Bash command for setting username in git **/

      snprintf(command, sizeof(buf), "git config --global user.name \"%s\"", username);
      system(command);

/** Save user details for convenience in cache **/

      strcpy(command, "git config --global credential.helper cache");
      system(command);

    }

    if ((choice == 3)||(choice == 4)) {

/** Prompts user to set link for CSGitlab remote repository **/

      printf("Please enter your CSGitlab remote URL:\n");
      scanf("%s", csgitlabLink);

/** Depending on whether they are new and want to add link or want to change link as there are different commands for those **/

      if (choice == 3) {

/** For adding link for the first time **/

        snprintf(command, sizeof(buf), "git remote add origin \"%s\"", csgitlabLink);
        system(command);

      }

      if (choice == 4) {

/** For changing exisiting link **/

        snprintf(command, sizeof(buf), "git remote set-url origin \"%s\"", csgitlabLink);
        system(command);

      }

    }

    if (choice == 5) {

/** For pushing their repository to CSGitlab, added set-upstream everytime just in case **/

      strcpy(command, "git push --set-upstream origin master");
      system(command);
      strcpy(command, "git push");
      system(command);

    }

    if (choice == 6) {

/** for getting their porjects from CSGitlab **/

      strcpy(command, "git pull");
      system(command);

    }

/** opens home and closes program **/

    if (choice == 7) {

      strcpy(command, "./home");
      system(command);

      return 0;

    }

  }

}
