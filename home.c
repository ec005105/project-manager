#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

/** Issue: For some reason, "cd" does not work with system() which makes accessing other git repositories impossible. One solution could be to implement another function other than system() to invoke "cd" or directly apply the function to the directory location. **/

int main () {

  int choice = 0;
  char command[50];
  bool choosing = true;
  int menu = 0;
  char buf[BUFSIZ];
  char gitName[30];

  while (choosing) {

    if (menu == 0) {

      strcpy(command, "cd programManager");
      system(command);
      strcpy(command, "clear");
      system(command);

      printf("Menu:\n");
      printf("1: Git\n");
      printf("2: Features\n");
      printf("3: Quit\n");

      scanf("%d", &choice);

      system(command);

      if (choice == 1) {

        menu = 1;

      }

      if (choice == 2) {

        menu = 2;

      }

/** Causes the program to close **/

      if (choice == 3) {

        exit(1);

      }

    }

    if (menu == 1) {

      printf("\n");
      printf("Git Features:\n");
      printf("1: create/delete git repository\n");
      printf("2: set up CSGitlab\n");
      printf("3: Manage branches\n");
      printf("4: Add/Commit\n");
      printf("5: back\n");

      scanf("%d", &choice);

      system(command);

      if (choice == 1) {

/** Opens the executable **/

        strcpy(command, "./gitcreate");
        system(command);
        choosing = false;

      }

      if (choice == 2) {

        printf("Git Repositories:\n");

/** Opens the file "gitfolder.txt" with fopen() in read mode **/

        FILE *fp = fopen("gitfolder.txt", "r");

/** Variable to store each line of string (name of created git repositories) in file **/

        char line[50];

/** Continues until end of text file **/

        while (fgets(line, sizeof(line), fp)) {

/** Prints each line of string individually from the text file **/

          printf("%s", line);

        }

/** Prompts user to selet git repository from the printed list**/

        printf("Select a git repository: ");
        scanf("%s", gitName);

/** Access available git repositories (cd does not work) **/

        snprintf(command, sizeof(buf), "cd %s", gitName);
        system(command);

        strcpy(command, "./csgitlab");
        system(command);
        choosing = false;

      }

      if (choice == 3) {

        menu = 3;

      }

      if (choice == 4) {

        menu = 4;

      }

      if (choice == 5) {

        menu = 0;

      }

    }

    if (menu == 2) {

      strcpy(command, "./feature");
      system(command);
      exit(1);

    }

    if (menu == 3) {

      strcpy(command, "./branch");
      system(command);
      exit(1);

    }

    if (menu == 4) {

      strcpy(command, "./commit");
      system(command);
      exit(1);

    }

  }

}
