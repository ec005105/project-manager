#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main() {

  char gitName[20];
  char command[150];
  char buf[BUFSIZ];
  bool repeatedName = false;
  char fileName[20];
  int choice;
  bool gitcreate = true;
  int menu = 1;

  while (gitcreate) {

    strcpy(command, "clear");
    system(command);

/* Opens gitfolder.txt file using fopen() */

    FILE *fp = fopen("gitfolder.txt", "r");

    char line [50];

/* Shows user list of existing git reposities from gitfolder.txt */

    printf("Git repositories:\n");

    while (fgets(line, sizeof(line), fp)) {

      printf("%s", line);

    }

    if (menu = 1) {

      printf("\n");
      printf("Create/Delete Git repository:\n");
      printf("1: Create Git repository\n");
      printf("2: Delete Git repository\n");
      printf("3: Back\n");

      scanf("%d", &choice);

      system(command);

      if (choice == 1) {

        menu = 0;

      }

      if (choice == 2) {

        menu = 1;

      }

      if (choice == 3) {

        strcpy(command, "./home");
        system(command);
        exit(1);

      }

    }

    repeatedName = false;

/* Prompts user to enter the name of git repository to be created */

    printf("\n");
    printf("Enter the name of your git repository: ");
    scanf("%s", gitName);
    system(command);

    if (menu == 0) {

/* Opens gitfolder.txt where names of existing git repositories are stored */

      FILE *fp = fopen("gitfolder.txt", "r");

      char line[100];

/* Compares the new git name to each name in the file */

      while (fgets(line, sizeof(line), fp)) {

/* If they match, repeatedName variable is set to true */

        if (strcmp(fileName,line) != 0) {

          repeatedName = true;

/** Prompts user to choose whether to create another name or abort */

          printf("Git repository name already exists.\n");
          printf("Would you like to use another name?\n");
          printf("1: Yes\n");
          printf("2: No\n");

          scanf("%d", &choice);

          strcpy(command, "clear");
          system(command);

/** If not abort, will continue while loop and prompt user to enter another name and check name*/

          if (choice == 2) {

            printf("Aborting git repository creation...\n");

            repeatedName = true;

            gitcreate = false;

          }

        }

      }

      if (!repeatedName) {

          gitcreate = false;

      }

    }

/* For deletion of git repository */

    if (menu == 1) {

      bool fileFound = false;

/* Pointer for two files for process of deleting a single line of string */

      FILE *fp2;

/* Points to gitfolder.txt and read it using fopen() */

      FILE *fp = fopen("gitfolder.txt", "r");

      char line[50];

/* Creates a file called replica.txt to be used in copying only undeleted names of git repositoru names */

      fp2 = fopen("replica.txt", "w");

/* Compares each line of string (names of git repositories) in text file with user inputted git Name */

      while (fgets(line, sizeof(line), fp)) {

/* Copies git Name from text file into replica file if they don't match */

        if (strcmp(gitName, line) == 0) {

          fputs(line, fp2);

/* Sets fileFound to true if a line of string matches with the git name */

        }

        if (strcmp(gitName, line) != 0) {

          fileFound = true;

        }

      }

      fclose(fp);
      fclose(fp2);

/* Deletes the old gitfolder.txt */

      remove("gitfolder.txt");

/* Renames the replica file into the original file's name */

      rename("replica.txt","gitfolder.txt");

      if (fileFound) {

/* Will remove git repository if file is found during comparison */

        snprintf(command, sizeof(buf), "rm -r -f %s", gitName);
        system(command);

      }

    }

  }
/** Takes variable gitName and copies it along with command to be used in system() */

  if (!repeatedName) {

/* Creates git repository */

    snprintf(command, sizeof(buf), "git init %s", gitName);
    system(command);

/* Adds name of git repository into gitfolder.txt to be used again in comparison */

    snprintf(command, sizeof(buf), "echo %s >> gitfolder.txt", gitName);
    system(command);

    snprintf(command, sizeof(buf), "cd %s", gitName);
    system(command);

    strcpy(command, "git add -A");
    system(command);

    strcpy(command, "git commit -a");
    system(command);

  }

/** Program will go back to home prompt */

  strcpy(command, "./home");
  system(command);

  exit(1);

}
