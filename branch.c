#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

int main () {

  bool choosing = true;
  int choice = 0;
  int menu = 0;
  char command[50];

  while (choosing) {

    if (menu == 0) {

/* Copies the string "clear" into command for it to be used in system() which acts like inputting bash commands */

      strcpy(command, "clear");
      system(command);

      printf("Manage branches:\n");
      printf("1: Create branches\n");
      printf("2: Delete branches\n");
      printf("3: Rename branches\n");
      printf("4: Back\n");

      scanf("%d", &choice);

      strcpy(command, "clear");
      system(command);

/* Options 1, 2 and 3 requires the user to select a git repository */

      if ((choice == 1)||(choice == 2)||(choice == 3)) {

        printf("Git repositories:\n");

/* Opens gitfolder.txt using fopen() in read mode */

        FILE *fp = fopen("gitfolder.txt", "r");

/* Variable for storing each line of string in text file */

        char line[50];

/** Prints each line of string (names of existing git repositories) **/

        while (fgets(line, sizeof(line), fp)) {

          printf("%s", line);

        }

        char branchName[30];
        char gitName[30];
        char buf[BUFSIZ];

/** Prompts user to select a git repository from list**/

        printf("Select a git repository: ");

        scanf("%s", gitName);

/** Should access git repository to do rest of functions in but cd does not work **/

        snprintf(command, sizeof(buf), "cd %s", gitName);
        system(command);

        strcpy(command, "clear");
        system(command);

/** Shows user all the branches in git repository **/

        strcpy(command, "git branch");
        system(command);

        if (choice == 1) {

/** Prompts user to enter new branch name **/

          printf("Enter the name of the new branch: ");
          scanf("%s", branchName);

/** copies string with variable and stores it in command **/

          snprintf(command, sizeof(buf), "git branch %s", branchName);

/** Creates new branch with user inputted name using system() **/
          system(command);

        }

        if (choice == 2) {

/** Prompts user to enter name of branch to be deleted from the printed list **/

          printf("Enter the name of the branch to be deleted: ");
          scanf("%s", branchName);

/** Deletes the branch name from user input **/

          snprintf(command, sizeof(buf), "git branch -D %s", branchName);
          system(command);

        }

        if (choice == 3) {

/** Prompts user to enter name of branch to be renamed **/

          printf("Enter the name of the branch to be renamed: ");
          scanf("%s", branchName);

          system(command);

/** Variable to store new branch name from user input **/

          char newBranchName[30];

/** Shows user all branches for reference **/

          strcpy(command, "git branch");

/** Prompts user to enter a new name for the branch **/

          printf("Enter new name for branch: ");
          scanf("%s", newBranchName);

/** Renames branch using two variables (old branch name and new branch name) **/

          snprintf(command, sizeof(buf), "git branch -m %s %s", branchName, newBranchName);
          system(command);

        }

      }

      if (choice == 4) {

/** Opens home and closes program **/

        strcpy(command, "./home");
        system(command);

        exit(1);

      }

    }

  }

}
