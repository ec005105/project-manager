gitcreate: gitcreate.c
	gcc gitcreate.c -o gitcreate

home: home.c
	gcc home.c -o home

csgitlab: csgitlab.c
	gcc csgitlab.c -o csgitlab

branch: branch.c
	gcc branch.c -o branch

feature: feature.c
	gcc feature.c -o feature

commit: commit.c
	gcc commit.c -o commit
