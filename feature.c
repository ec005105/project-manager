#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main() {

  bool feature = true;
  int menu = 0;
  char command[50];
  int choice = 0;

  while (feature) {

    if (menu == 0) {

      strcpy(command, "clear");
      system(command);

      printf("Features:\n");
      printf("1: Create feature\n");
      printf("2: Delete feature\n");
      printf("3: Edit feature\n");
      printf("4: Back\n");

      scanf("%d", &choice);

      strcpy(command, "clear");
      system(command);

/* Options 1, 2 and 3 requires user to select a git repository */

      if ((choice == 1)||(choice == 2)||(choice == 3)) {

/* prints list of git repositories stored in text file */

        printf("Git repositories:\n");

        FILE *fp = fopen("gitfolder.txt", "r");

        char line[50];

        while (fgets(line, sizeof(line), fp)) {

          printf("%s", line);

        }

        char featureName[30];
        char branchName[30];
        char gitName[30];
        char buf[BUFSIZ];

/* Prompts user to select a git repository from the list */

        printf("Select a git repository: ");
        scanf("%s", gitName);

/* Should access selected git repository but cd does not work */

        snprintf(command, sizeof(buf), "cd %s", gitName);
        system(command);

/* Shows user all branches in git repository */

        strcpy(command, "git branch");
        system(command);

/* Prompts user to choose branch they want to access */

        printf("Enter name of branch to access: ");
        scanf("%s", branchName);

        snprintf(command, sizeof(buf), "git switch %s", branchName);
        system(command);

/* Shows list of features in the git repository */

        printf("Features:\n");

        strcpy(command, "ls");
        system(command);

        if (choice == 1) {

/* Prompts user to enter a new feature name with reference */

          printf("Enter name of feature to add: ");
          scanf("%s", featureName);

/* Creates the file using nano */

          snprintf(command, sizeof(buf), "nano %s/%s", gitName, featureName);
          system(command);

        }

        if (choice == 2) {

/* Prompts user to enter name of feature to be deleted with reference */

          printf("Enter name of feature to delete: ");
          scanf("%s", featureName);

          snprintf(command, sizeof(buf), "rm %s/%s", gitName, featureName);
          system(command);

        }

/* Prompts user to enter the name fo feature to be editted */

        if (choice == 3) {

          printf("Enter name of feature to edit: ");
          scanf("%s", featureName);

/* Shows user directory of the feature for reference from $HOME */

          printf("Here is the directory of the feature:\n");
          snprintf(command, sizeof(buf), "find $HOME -name \"%s\"", featureName);
          system(command);

          int subChoice = 0;

/* Prompts user to either move the feature or efit the contents of the feature */

          printf("1: move feature\n");
          printf("2: edit contents\n");
          scanf("%d", &subChoice);

          strcpy(command, "clear");
          system(command);

          if (subChoice == 1) {

/* Prompts user to enter branch name for the file to be moved into */

            printf("Enter name of branch for file to be moved into: ");
            scanf("%s", branchName);

/* Bash command for movinf files between branches */

            snprintf(command, sizeof(buf), "git checkout %s %s", branchName, featureName);
            system(command);

          }

          if (subChoice == 2) {

/* Bash command for editting file */

            snprintf(command, sizeof(buf), "nano %s/%s", gitName, featureName);
            system(command);

          }

        }

      }

/* Opens home and closes the program */

      if (choice == 4) {

        strcpy(command, "./home");
        system(command);
        exit(1);

      }

    }

  }

}
