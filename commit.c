#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main() {

  bool commit = true;
  char command[150];
  char buf[BUFSIZ];
  char message[150];

  while (commit) {

/** Adds changes made to the git repository **/

    strcpy(command, "git add -A");
    system(command);

/** Commits all the changes and will automatically prompt user to enter a commit message **/

    strcpy(command, "git commit -a");
    system(command);

/** Opens home and closes the program **/

    strcpy(command, "./home");
    system(command);

    exit(1);

  }

}
